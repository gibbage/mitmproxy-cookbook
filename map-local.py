from mitmproxy import flowfilter
from mitmproxy import ctx, http

class MapLocal:

    def __init__(self):
        self.filter = None # type: flowfilter.TFilter
        self.local_path = None
        self.content_type = None

    def configure(self, updated):
        self.filter = flowfilter.parse(ctx.options.flowfilter)
        self.local_path = ctx.options.maplocalpath
        self.content_type = ctx.options.maplocalcontenttype

    def load(self, l):
        l.add_option(
            "flowfilter", str, "", "Check that flow matches filter."
        )
        l.add_option(
            "maplocalpath", str, "", "Path containing new Response Body."
        )
        l.add_option(
            "maplocalcontenttype", str, "", "Content-type of new Response Body."
        )

    def response(self, flow: http.HTTPFlow) -> None:
        if flowfilter.match(self.filter, flow):
            print("Flow matches filter:")
            print(flow)

            flow.response.headers["local-override"] = "true"
            if self.content_type:
              flow.response.headers["Content-Type"] = self.content_type

            local_file = open(self.local_path, "r")
            flow.response.content = str(local_file.read()).encode("utf8")

addons = [MapLocal()]
