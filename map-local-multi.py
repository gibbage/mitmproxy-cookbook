import json
from mitmproxy import flowfilter
from mitmproxy import ctx, http

class MapLocal:

    def __init__(self):
        self.config = None
        self.filters = []

    def configure(self, updated):
        if ctx.options.maplocalconfigpath:
            self.config = json.load(open(ctx.options.maplocalconfigpath))
            for mapping in self.config:
                self.filters.append(flowfilter.parse(mapping['flow-filter']))

    def load(self, l):
        l.add_option(
            "maplocalconfigpath", str, "", "Config file for Map Local"
        )

    def response(self, flow: http.HTTPFlow) -> None:
        for i in range(len(self.filters)):
            if flowfilter.match(self.filters[i], flow):
                print("We have a match for:")
                print(flow)
                flow.response.headers["local-override"] = "true"
                print(self.config[i])
                if 'content-type' in self.config[i]:
                    flow.response.headers["Content-Type"] = self.config[i]['content-type']
                if 'local-path' in self.config[i]:
                    local_file = open(self.config[i]['local-path'], "r")
                    flow.response.content = str(local_file.read()).encode("utf8")

addons = [MapLocal()]
