# mitmproxy scripts

I love Charles Web Debugging Proxy but find I'm always lost in it's UI for
trying to set things up.

I recently discovered another project - [mitmproxy](https://mitmproxy.org/)
which provides a few different tools to help you do common development and
testing things from the command line.

It provides a powerful addon scripting library to allow you to easily write
python scripts for all your common use cases.

## Proxy cookbook

### Make your thing SSL!

    mitmdump -p 443 --mode reverse:http://localhost:8000/

### Manipulate the RESPONSE headers

    mitmdump --setheaders :~s:Server:tomcat # modify existing
    mitmdump --setheaders :~s:foo:bar       # ... or add one if it's not there

### Manipulate the RESPONSE body

    mitmdump --replacements :~s:html:wat    # Replace <html> tags with <wat>

### Map response to local file

    mitmdump --scripts ./map-local.py --set flowfilter="~u google" --set maplocalpath="./hero.json"

### Map response to local file and update it's Content-Type

    mitmdump --scripts ./map-local.py --set flowfilter="~u google" --set maplocalpath="./hero.json" --setheaders :~s:content-type:application/json

### Map multiple responses to local files

    mitmdump --scripts ./map-local-multi.py --set maplocalconfigpath="map-local-config.json"

Example content for `map-local-config.json`:

```json
[
  {
    "flow-filter": "~u takeover",
    "content-type": "application/json",
    "local-path": "./hero.json"
  },
  {
    "flow-filter": "~u v60.js",
    "local-path": "./crap.js"
  }
]
```

### Throttle request

TODO

### Blockhole request

TODO
